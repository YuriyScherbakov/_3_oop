﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_HomeWork3.myAnimals
{
   
    interface IForUnderWaterThings
    {
       string breatheUnderWater();
    }
    interface IForHerbivorous
    {
        string eatHay();
    }

    abstract class LivingThings 
    {
        
        public string Name {get;set;}

       
    }

    class Animals : LivingThings
    {
        int LegsNumber = 4;
        public int legsNumber
        {
            set
            {
                if ( value > 0 )
                {
                    this.LegsNumber = value;
                }
            }
            get
            {
                return this.LegsNumber;
            }
        }

    }

    class Horse : Animals, IForHerbivorous
    {
        public string eatHay()
        {
            return ("Ем сено.");
        }
    }
    class Dog : Animals, IForUnderWaterThings
    {
        public string breatheUnderWater()
        {
            return ( "Я дышу под водой." );
        }
    }
    class Fish : LivingThings, IForUnderWaterThings

    {
        int LegsNumber = 0; // Бывают рыбы с ногами

        public string breatheUnderWater()
        {
            return("Я дышу под водой.");
        }
        public int legsNumber
        {
            set
            {
                if ( value > 0 )
                {
                    this.LegsNumber = value;
                }
            }
            get
            {
                return this.LegsNumber;
            }
        }
    }
    class Сrucian : Fish, IForHerbivorous //Карась
    {
        public string eatHay()
        {
            return ( "Ем сено." );
        }
    }
    class Roach : Fish //Плотва
    {
    }
}
