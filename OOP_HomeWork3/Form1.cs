﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OOP_HomeWork3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<LivingThings> myLivingThings;
        List<LivingThings> myUnderWaterThings;


        private void button1_Click(object sender,EventArgs e)
        {
            myLivingThings =  LivingThingsManipulator.AddLivingThings(new List<LivingThings>());
            labelNumberOfLegs.Text = LivingThingsManipulator.CountLegs(myLivingThings).ToString();
            textBox1.Clear();
            textBox1.AppendText("Мы умеем дышать под водой.");

            textBox1.AppendText(Environment.NewLine);
            textBox1.AppendText(Environment.NewLine);

            myUnderWaterThings = LivingThingsManipulator.FindIForUnderWaterThings(myLivingThings);

            foreach ( LivingThings item in myUnderWaterThings )
            {
                textBox1.AppendText(item.Name);
                textBox1.AppendText(Environment.NewLine);
            }
            
        }
    }
}
