﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_HomeWork3
{
    class Animals : LivingThings
    {
        public Animals()
        {
            this.legsNumber = 4;
        }

        int legsNumber;
        public int LegsNumber
        {
            set
            {
                if ( value > 0 )
                {
                    this.legsNumber = value;
                }
            }
            get
            {
                return this.legsNumber;
            }
        }

    }
}
