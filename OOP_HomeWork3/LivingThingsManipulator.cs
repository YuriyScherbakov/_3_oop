﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_HomeWork3
{
   static class LivingThingsManipulator
    {
      public static  List<LivingThings> AddLivingThings(List<LivingThings> myLivingThings)
        {

            myLivingThings.Clear();
            myLivingThings.Add(new Сrucian() { Name = "Буль-буль(Карась)" });

            myLivingThings.Add(new Dog() { Name = "Эклер(Собака)" });
            myLivingThings.Add(new Horse() { Name = "Михалыч(Лошадь)" });

            myLivingThings.Add(new Dog() { Name = "Наполеон(Собака)" });

            myLivingThings.Add(new Horse() { Name = "Ребби(Лошадь)" });
            myLivingThings.Add(new Roach() { Name = "Льюис(Плотва)" });

            myLivingThings.Add(new Сrucian() { Name = "Карамелька(Карась)" });

            myLivingThings.Add(new Dog() { Name = "Буч(Собака)" });
            myLivingThings.Add(new Horse() { Name = "Штраус(Лошадь)" });

            myLivingThings.Add(new Dog() { Name = "Джо(Собака)" });

            myLivingThings.Add(new Horse() { Name = "Эклер(Лошадь)" });
            myLivingThings.Add(new Roach() { Name = "Пиксель(Плотва)" });

            return myLivingThings;

        }
        public static int CountLegs(List<LivingThings> myLT)
        {
            int amountOfLegs = 0;
            foreach ( var animal in myLT )
            {
                if ( animal is Animals )
                {
                    amountOfLegs += ( animal as Animals ).LegsNumber;
                }

            }

            return amountOfLegs;
        }
        public static List<LivingThings> FindIForUnderWaterThings (List<LivingThings> myLivingThings)
        {
            List<LivingThings> myUnderWaterThings = new List<LivingThings>();

           foreach ( LivingThings myLT in myLivingThings )
            {
                if ( myLT is IUnderWaterThings )
                {
                    myUnderWaterThings.Add(myLT);
                }
            }

            return myUnderWaterThings;

        }

    }
}
