﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_HomeWork3
{
    class Dog : Animals, IUnderWaterThings
    {
        public string BreatheUnderWater()
        {
            return ( "Я дышу под водой." );
        }
    }
}
